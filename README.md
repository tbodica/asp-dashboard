# asp-dashboard
ASP Dashboard with authentication, DB interaction, and chat.

![Login](Images/login.png)
![Publications](Images/publications.png)
![Chat](Images/chat.png)
![Authenticate](Images/authenticate.png)