﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;

using Microsoft.Extensions.Configuration;



namespace WebApplication1_mvc.Models
{
    public class LibraryBook
    {
        private string Name;
        private string ISBN;

        public LibraryBook(LibraryBook k)
        {
            this.Name = k.Name;
            this.ISBN = k.ISBN;
        }
        public LibraryBook(string ISBN, string Name)
        {
            this.ISBN = ISBN;
            this.Name = Name;
        }

        public string GetISBN()
        {
            return this.ISBN;
        }
        public override string ToString()
        {
            return String.Format("{0} | {1}", this.ISBN, this.Name);
        }
    }

    public class BookRepo
    {
        public List<LibraryBook> booksCollection { get; }
        private string connectionString;
        private string bookTableName = "publications_tb";
        MySqlConnection mySqlConnection;

        private IConfiguration SqlConfiguration;

        public BookRepo()
        {
            booksCollection = new List<LibraryBook>();

            SqlConfiguration = new ConfigurationBuilder()
                .AddJsonFile("sqlSettings.json", optional: false, reloadOnChange: false)
                .Build();

            connectionString = String.Format("server=localhost;port=3306;userid={0};database=library_pdf;password={1}",
                SqlConfiguration.GetSection("Credentials").GetValue(typeof(string), "username"),
                SqlConfiguration.GetSection("Credentials").GetValue(typeof(string), "password")
                );
        }

        public void Add(LibraryBook book)
        {
            booksCollection.Add(new LibraryBook(book));
        }

        public void Remove(LibraryBook book)
        {
            foreach (var Ibook in booksCollection)
            {
                if (book == Ibook)
                {
                    booksCollection.Remove(book);
                }
            }
        }

        public string Get(int index)
        {
            var Ibook = booksCollection[index];
            return Ibook.ToString();
        }

        public List<LibraryBook> GetAll()
        {
            return booksCollection;
        }

        public void Populate()
        {
            mySqlConnection = new MySqlConnection(connectionString);
            mySqlConnection.Open();

            string queryString = "SELECT DOI, Name FROM " + bookTableName;
            var cmd = new MySqlCommand(queryString, mySqlConnection);
            var reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                string ISBN = reader.GetString(0);
                string Name = reader.GetString(1);
                booksCollection.Add(new LibraryBook(ISBN, Name));
            }

            reader.Close();
            mySqlConnection.Close();
        }
    }
}
