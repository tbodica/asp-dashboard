﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication1_mvc.Models;
using MySql.Data;
using MySql.Data.MySqlClient;
using ASP_Dashboard.Chatbots;
using SignalRChat.Hubs;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNetCore.SignalR;

namespace WebApplication1_mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHubContext<ChatHub> hubContext;

        public HomeController(ILogger<HomeController> logger, IHubContext<ChatHub> hubContext)
        {
            System.Console.WriteLine("HomeController instance"); // #WriteLine
            _logger = logger;
            this.hubContext = hubContext;
            System.Console.WriteLine(this.hubContext.ToString()); // #WriteLine
        }

        public IActionResult Index()
        {
            BookRepo bookRepo = new BookRepo();
            bookRepo.Populate();

            return View(bookRepo);
        }

        public IActionResult Chat()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
