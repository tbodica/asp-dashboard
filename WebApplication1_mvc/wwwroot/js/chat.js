﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/chatHub").build();

//Disable send button until connection is established
document.getElementById("sendButton").disabled = true;

connection.on("ReceiveMessage", function (user, message) {
    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = user + ": " + msg;
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
    var listElements = document.getElementById("messagesList").getElementsByTagName("li");
    listElements[listElements.length - 1].scrollIntoView();
    
});

connection.start().then(function () {
    document.getElementById("sendButton").disabled = false;
    connection.invoke("PopulateChat");
}).catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var user = document.getElementById("userInput").value;
    var message = document.getElementById("messageInput").value;
    connection.invoke("SendMessage", user, message).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

function keyPressHandler(e) {
    if (e.keyCode === 13) {
        var user = document.getElementById("userInput").value;
        var message = document.getElementById("messageInput").value;
        connection.invoke("SendMessage", user, message, false).catch(function (err) {
            return console.error(err.toString());
        });
        document.getElementById("messageInput").value = "";
        e.preventDefault();
    }
}