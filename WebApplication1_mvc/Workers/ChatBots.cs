﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging; // todo read
using SignalRChat.Hubs;
using ASP_Dashboard.Chatbots;

namespace ASP_Dashboard.Workers
{
    public class ChatBots : IHostedService, IDisposable
    {
        private IHubContext<ChatHub> hubContext;
        private Chatbot masterBot;

        public ChatBots(IHubContext<ChatHub> hubContext)
        {
            this.hubContext = hubContext;
            System.Console.WriteLine("mycontex " + this.hubContext.ToString()); // #WriteLine
            this.masterBot = masterBot = new Chatbot("masterbot", 5);
            this.masterBot.BindMethod(SendStatus);
        }

        private bool SendStatus()
        {
            DateTime botTime = DateTime.Now;

            var messageString = String.Format("{0} I like to tell the time every 15 seconds.", botTime.ToString("yyyy-MM-d HH:mm:ss"));
            var chatMessage = new ChatMessage(this.masterBot.Name, messageString, botTime);
            botTime = DateTime.Now;

            var sendTask = hubContext.Clients.All.SendAsync("ReceiveMessage", this.masterBot.Name, String.Format("{0} I like to tell the time every 15 seconds.", botTime.ToString("yyyy-MM-d HH:mm:ss")), false);
            try
            {
                sendTask.Wait();
            }
            catch
            {
                System.Console.WriteLine("Send task failed");
                return false;
            }
            
            return true;
        }

        public void Dispose()
        {
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}
