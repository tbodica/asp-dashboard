﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging; // todo read

//using MySql.Data;
using MySql.Data.MySqlClient;

using System.Net;
using System.Text.RegularExpressions;

using Microsoft.Extensions.Configuration;


namespace WebApplication1_mvc.Workers
{
    public abstract class IDb_Http_Worker : IHostedService, IDisposable
    {
        internal string webSourceUrl;
        internal string matchPattern;
        internal string tableName;
        internal string insertQueryString;
        internal string tableColumnName;

        private int executionCount = 0;
        private Timer _timer;

        MySqlConnection mySqlConnection;
        private IConfiguration SqlConfiguration;

        public IDb_Http_Worker()
        {
            SqlConfiguration = new ConfigurationBuilder()
                .AddJsonFile("sqlSettings.json", optional: false, reloadOnChange: false)
                .Build();

            var connectionString = String.Format("server=localhost;port=3306;userid={0};database=mining;password={1}",
                SqlConfiguration.GetSection("Credentials").GetValue(typeof(string), "username"),
                SqlConfiguration.GetSection("Credentials").GetValue(typeof(string), "password")
                );

            mySqlConnection = new MySqlConnection(connectionString);
            SetSourceAndPattern();
        }

        public abstract void SetSourceAndPattern();

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(36000));

            return Task.CompletedTask;
        }

        private void DoWork(object state)
        {
            var count = Interlocked.Increment(ref executionCount);

            mySqlConnection.Open();
            var htmlRegexParseValueString = "-1";

            using (WebClient client = new WebClient())
            {
                string documentHtmlString = client.DownloadString(this.webSourceUrl);
                Regex regexCompiled = new Regex(this.matchPattern, RegexOptions.Compiled);
                MatchCollection matches = regexCompiled.Matches(documentHtmlString);

                if (matches.Count == 1)
                {
                    System.Console.WriteLine(matches[0]); // #WriteLine
                    string valueAsString = matches[0].ToString(); // !!! TODO: refactor to insert for each match, and find way to test worker / code 
                    htmlRegexParseValueString = this.FormatFoundValue(valueAsString); // need to format with override method instead
                }
                else
                {
                    mySqlConnection.Close();
                    return;
                }

            }

            var timeNow = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var queryString = this.GetQueryString(timeNow, htmlRegexParseValueString.ToString());
            MySqlCommand cmd = new MySqlCommand(queryString, mySqlConnection);
            var reader = cmd.ExecuteReader();
            reader.Read();
            reader.Close();


            mySqlConnection.Close();

        }

        public abstract String FormatFoundValue(string regexMatch);

        public abstract String GetQueryString(string timeNow, string htmlFoundValue);

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }

    }
    public class CNLR_Db_Worker : IDb_Http_Worker
    {
        public CNLR_Db_Worker() : base()
        {

        }

        public override void SetSourceAndPattern()
        {
            this.webSourceUrl = "https://www.pbinfo.ro/solutii";
            this.matchPattern = @"[0-9]+ solu";
            this.tableName = "cnlr";
            this.insertQueryString = "";
            this.tableColumnName = "pbinfo_solutions";
        }

        public override String FormatFoundValue(string regexMatch)
        {
            return regexMatch.Split(" ")[0];
        }
        public override String GetQueryString(string timeNow, string htmlFoundValue)
        {
            return String.Format("INSERT INTO {2}(timestamp, {3}) VALUES (\"{0}\", \"{1}\")", timeNow, htmlFoundValue, this.tableName, this.tableColumnName);
        }

    }
}
