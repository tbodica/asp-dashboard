﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.VisualStudio.Web.CodeGeneration.Contracts.Messaging;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Threading.Tasks;
using System.Transactions;

using System.Data.SQLite;
using ASP_Dashboard.Chatbots;

namespace SignalRChat.Hubs
{
    public class ChatMessage
    {
        public string userName { get; }
        public string message { get; }

        public DateTime date { get; }

        public ChatMessage(string userName, string message, DateTime date)
        {
            this.userName = userName;
            this.message = message;
            this.date = date;
        }

        public override string ToString()
        {
            return date.ToString("HH:mm:ss") + " : " + userName + ": " + message;
        }

    }
    public class ChatHub : Hub
    {
        private List<ChatMessage> messageLog;
        private SQLiteConnection chatSQLiteConnection;

        // todo: inject sql connection, add another connection to bots, or simply use parent class for connection
        public ChatHub() : base()
        {
            System.Console.WriteLine("ChatHub init"); // todo: Debug // #WriteLine
            messageLog = new List<ChatMessage>();

            if (!System.IO.File.Exists("chatDB.sqlite"))
            {
                SQLiteConnection.CreateFile("chatDB.sqlite"); 
                chatSQLiteConnection = new SQLiteConnection("Data Source=chatDB.sqlite;Version=3;");
                chatSQLiteConnection.Open();

                string createTableString = "CREATE TABLE messages (username varchar(128), message varchar(4096), date default current_timestamp)";
                SQLiteCommand command = new SQLiteCommand(createTableString, chatSQLiteConnection);
                command.ExecuteNonQuery();
                chatSQLiteConnection.Close();
            }

            chatSQLiteConnection = new SQLiteConnection("Data Source=chatDB.sqlite;Version=3;");
            chatSQLiteConnection.Open();

            GetMessagesFromDB();
        }


        public void PopulateChat()
        {
            foreach (var msg in messageLog)
            {
                System.Console.WriteLine("Populated from PopulateChat()"); // #WriteLine
                var smTask = SendMessage(msg.userName, msg.message, initializing: true);
                smTask.Wait();
            }
        }

        public void GetMessagesFromDB()
        {
            messageLog.Clear();

            var queryString = "SELECT * FROM messages";
            SQLiteCommand command = new SQLiteCommand(queryString, chatSQLiteConnection);
            var reader = command.ExecuteReader();

            while (reader.Read())
            {
                var username = reader.GetString(0);
                var message = reader.GetString(1);
                var chatMessage = new ChatMessage(username, message, DateTime.Parse(reader.GetString(2)));
                messageLog.Add(chatMessage);
            }
            reader.Close();
        }

        public void AddMessageToDB(ChatMessage chatMessage)
        {
            SQLiteCommand command = new SQLiteCommand(chatSQLiteConnection);
            command.CommandText = "INSERT INTO messages(username, message, date) VALUES (@username, @message, @date)";
            command.Parameters.AddWithValue("username", chatMessage.userName);
            command.Parameters.AddWithValue("message", chatMessage.message);
            command.Parameters.AddWithValue("date", chatMessage.date.ToString("yyyy-MM-dd HH:mm:ss"));
            command.ExecuteNonQuery();
        }

        public void AddMessage(ChatMessage chatMessage)
        {
            AddMessageToDB(chatMessage);
            messageLog.Add(chatMessage);
        }

        public async Task SendMessage(string user, string message, bool initializing)
        {

            if (!initializing)
            {
                AddMessage(new ChatMessage(user, message, new DateTime()));
            }
            
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        public List<ChatMessage> GetAllMessages()
        {
            return messageLog;
        }
    }
}