using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using WebApplication1_mvc.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using WebApplication1_mvc.Workers;
using SignalRChat.Hubs;
using Microsoft.AspNetCore.Routing.Matching;
using MySql.Data.EntityFrameworkCore;

namespace WebApplication1_mvc
{
    public class Startup
    {
        private IConfiguration SqlConfiguration;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            SqlConfiguration = new ConfigurationBuilder()
                .AddJsonFile("sqlSettings.json", optional:false, reloadOnChange:false)
                .Build();
            using (var client = new ApplicationDbContext())
            {
                client.Database.EnsureCreated();
                //client.Database.Migrate();
            }
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlite().AddDbContext<ApplicationDbContext>();
            //services.AddDbContext<ApplicationDbContext>(options =>
            //    options.UseMySQL(
            //        Configuration.GetConnectionString("MariaDBConnection")));
            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddAuthorization(options =>
            {
                options.AddPolicy("Administrator", policy =>
                    policy.RequireRole("Administrator")
                );

                options.AddPolicy("User", policy =>
                    policy.RequireRole("User")
                );

            });
            
            services.Configure<IdentityOptions>(options =>
            {
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                options.User.RequireUniqueEmail = true;
                options.SignIn.RequireConfirmedAccount = true;
            });

            services.AddSignalR();
            // todo: possibly will need to add another service for non-duplicate bots
            //services.AddHostedService<CNLR_Db_Worker>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
                endpoints.MapHub<ChatHub>("/chatHub");
            });
            
            ConfigureRoles(serviceProvider);
        }

        private void ConfigureRoles(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            string[] roleNames = { "Administrator", "User", "Bot" };
            Task<IdentityResult> roleResult;

            // email: role
            Dictionary<String, String> specialUsers = new Dictionary<String, String>();
            specialUsers.Add("tbodica@gmail.com", "Administrator");
            // specialUsers.Add("tbodica@gmail.com", "User"); might not be ok
            specialUsers.Add("sample@sample.com", "User");
            specialUsers.Add("NotExists@YET.com", "User");


            // list users
            System.Console.WriteLine("Printing users: \r\n"); // #WriteLine
            foreach (var user in UserManager.Users)
            {
                System.Console.WriteLine(user.Email.ToString()); // #WriteLine
            }

            // create new roles with RoleManager<>
            foreach (var roleName in roleNames)
            {
                var roleExist = RoleManager.RoleExistsAsync(roleName);
                roleExist.Wait();
                System.Console.WriteLine(roleExist.Result.ToString()); // #WriteLine
                if (roleExist.Result == false)
                {
                    //create the roles and seed them to the database: Question 1
                    roleResult = RoleManager.CreateAsync(new IdentityRole(roleName));
                    roleResult.Wait();
                    System.Console.WriteLine("Added role: " + roleName.ToString()); // #WriteLine
                }
            }

            // check if users have roles
            UpdateUserRoles(specialUsers, roleNames, UserManager, RoleManager);

        }

        public void UpdateUserRoles(Dictionary<String, String> specialUsers, string[] roleNames, UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            foreach (KeyValuePair<string, string> entry in specialUsers)
            {
                // check key, pair
                var userExists = userManager.FindByEmailAsync(entry.Key);
                userExists.Wait();
                if (userExists.Result == null)
                {
                    System.Console.WriteLine("User " + entry.Key + " does not exist."); // #WriteLine
                    continue;
                }

                var roleExists = roleManager.FindByNameAsync(entry.Value);
                roleExists.Wait();
                if (roleExists.Result == null)
                {
                    System.Console.WriteLine("Role " + entry.Value + " does not exist."); // #WriteLine
                    continue;
                }

                var userObject = userExists.Result; // the user in UserManager
                var roleObject = roleExists.Result; // the role to be added in RoleManager

                var existingRolesOfUser = userManager.GetRolesAsync(userObject);
                existingRolesOfUser.Wait();

                bool alreadyHas = false;
                foreach (var roleName in existingRolesOfUser.Result)
                {
                    if (roleName == roleObject.Name)
                        alreadyHas = true;
                }

                if (!alreadyHas)
                {
                    IdentityResult addResult = userManager.AddToRoleAsync(userObject, roleObject.Name).Result;
                    if (addResult == IdentityResult.Success)
                        System.Console.WriteLine("Successfully added " + roleObject.Name.ToString() + " to user: " + userObject.Email); // #WriteLine
                }
                else
                {
                    System.Console.WriteLine("Role " + roleObject.Name.ToString() + " already belongs to user: " + userObject.Email); // #WriteLine
                }

            }
        }
    }
}
