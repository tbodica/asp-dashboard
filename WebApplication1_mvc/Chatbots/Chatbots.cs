﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace ASP_Dashboard.Chatbots
{
    public class Chatbot
    {
        public string Name { get; }
        private System.Timers.Timer botTimer;
        private Func<bool> botBoundMethod;
        private bool Success;

        public Chatbot(string Name, int Seconds)
        {
            int milliseconds = Seconds * 1000;
            SetTimer(milliseconds);

            this.Name = Name;
        }

        private void SetTimer(int milliseconds)
        {
            this.botTimer = new System.Timers.Timer((float)milliseconds);
            this.botTimer.Start();

            this.botTimer.Elapsed += TimerEvent;
        }

        private void TimerEvent(Object source, ElapsedEventArgs e)
        {
            botTimer.Stop();
            botTimer.AutoReset = true;
            botTimer.Start();
            botTimer.Enabled = true;
            if (this.botBoundMethod != null)
            {
                this.Success = this.botBoundMethod();
                if (this.Success)
                    System.Console.WriteLine("Success in writing time."); // #WriteLine
            }
            System.Console.WriteLine("One cycle performed."); // #WriteLine
        }

        public void BindMethod(Func<bool> _BoundMethod)
        {
            this.botBoundMethod = _BoundMethod;
        }
    }
}
